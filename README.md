Climbsaber - map recommendation tool for Beat Saber
==============================

Web app using recommendation system for ranked maps in Beat Saber. \
Climbsaber can predict using two options:
1) Machine learning model based on over 230 metrics like blocks in minute or count of duck walls :D
2) System can predict how much you will receive performance point if you pass song like this guy. 

## Usage
1. Open webapp nowadays hosted [here](http://minecraft228.duckdns.org:8080/)
2. In sidebar input box enter your ScoreSaber ID
3. (Optional) Enter second ScoreSaber ID to compare with
4. If you want orient on other player plays. 
Select in dropdown pp change priority - *Opponent* or 
keep ML for machine learning prediction of your accuracy.
5. Customize filters for better experience
6. (Once. Beat saber playlist plugin required) Click download playlist 
and put in SteamLibrary\steamapps\common\Beat Saber\Playlists
7. In game, you can refresh list of maps by pressing sync button in playlist menu
![](https://media.discordapp.net/attachments/352156842836754433/964835053253296138/unknown.png?width=500&height=300)

ENJOY!

## Project status
This project is my pet, so I work on it in free time. Created for training skills in CI/CD. Course: https://ods.ai/tracks/ml-in-production-spring-22

Course Rerun: https://ods.ai/tracks/ml-in-production-spring-23

## Installation
1. python -m venv venv --p=python3.9\
enter in venv for windows: 
2. call venv/Scripts/activate\
3. pip install -r requirements.txt\
4. docker-compose -f docker-compose-infra.yaml -p climbsaber-infra up -d --build
Generate data:
5. dvc repro

After you need to run api:
6. python .\src\web\app_api.py\
And then main web app:
7. streamlit run .\src\web\app.py

OR use docker

6. docker build -f .\Docker\climbsaber_api\Dockerfile -t climbsaber_api .

7. docker build -f .\Docker\climbsaber_site\Dockerfile -t climbsaber_site .  

8. docker-compose up -d --build


Project Organization
------------

    ├── LICENSE
    ├── Makefile           <- Makefile with commands like `make data` or `make train`
    ├── README.md          <- The top-level README for developers using this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── docs               <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models             <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks          <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                         the creator's initials, and a short `-` delimited description, e.g.
    │                         `1.0-jqp-initial-data-exploration`.
    │
    ├── references         <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports            <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures        <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    ├── setup.py           <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                <- Source code for use in this project.
    │   ├── __init__.py    <- Makes src a Python module
    │   │
    │   ├── data           <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py
    │   │
    │   ├── visualization  <- Scripts to create exploratory and results oriented visualizations
    │   │    └── visualize.py
    │   ├── climbsaber  <- Scripts to work with api and other util modules
    │   │    ├── beatsaver.py  <- Gets information about songs and downloads them from beatsaver.com
    │   │    ├── prediction.py  <- Contains class for prediction how much performance points player will gain and other stats
    │   │    ├── scoresaber.py  <- Api wrapper for ScoreSaber.com
    │   │    ├── song_parser.py  <- Beat Saber song file parser. Generate features like num blocks in minute and etc
    │   │    └── util.py  <- Small usefull scripts
    │   └── web  <- web applications
    │       ├── app.py  <- Main web app based on streamlit
    │       └── app_api.py  <- Api based on flask
    │
    └── tox.ini            <- tox file with settings for running tox; see tox.readthedocs.io

Haha! That's my line. I deleted one character find it
