from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='map recommendation tool for Beat Saber',
    author='@m2sorokin',
    license='MIT',
)
