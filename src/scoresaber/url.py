def scores(player_id: str, page: int):
    return f"/api/player/{player_id}/scores?sort=top&page={page}&limit=100"


def players(page: int):
    return f"/api/players?page={page}"


def ranked_maps(page: int):
    return f"/api/leaderboards?ranked=1&page={page}"


def player_info(player_id: str):
    return f"/api/player/{player_id}/full"
