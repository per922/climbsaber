import asyncio
import logging
import math
from itertools import chain

from aiohttp import ClientSession
from asyncio_throttle import Throttler

from . import url


def parse_score(row, player_id):
    score = row["score"]
    score["leaderboardId"] = row["leaderboard"]["difficulty"]["leaderboardId"]
    score["playerId"] = player_id
    return score


def parse_scores_page(response, player_id):
    return map(
        lambda row: parse_score(row, player_id), filter(has_pp, response["playerScores"])
    )


def parse_ranked_maps(response):
    return response["leaderboards"]


def parse_players(response):
    return response["players"]


def has_pp(row):
    return row["score"]["pp"] > 0


def get_total_pages(metadata):
    return math.ceil(metadata["total"] / metadata["itemsPerPage"])


class ScoreSaberAsync:
    throttler = Throttler(rate_limit=300, period=60)

    async def __aenter__(self) -> "ScoreSaberAsync":
        self.session = ClientSession("https://scoresaber.com/")
        return self

    async def __aexit__(self, exc_t, exc_v, exc_tb) -> None:
        await self.session.close()

    async def __request(self, url):
        async with ScoreSaberAsync.throttler:
            try:
                async with self.session.get(url) as response:
                    return await response.json()
            except Exception as e:
                logging.error(f"error while parse {url} with response: {response} {e}")
                return dict()

    async def __get_scores_page(self, player_id: str, page: int):
        return await self.__request(url.scores(player_id, page))

    async def get_scores(self, player_id: str, max_pages: int = 5):
        first_page = await self.__get_scores_page(player_id, 1)
        total_pages = get_total_pages(first_page["metadata"])
        pages = min(total_pages, max_pages)
        result_pages = [
            self.__get_scores_page(player_id, page) for page in range(2, pages + 1)
        ]
        return chain.from_iterable(
            map(
                lambda page: parse_scores_page(page, player_id),
                [first_page] + await asyncio.gather(*result_pages),
            )
        )

    async def __get_players_page(self, page: int):
        return await self.__request(url.players(page))

    async def get_players(self, start_page: int, end_page: int):
        result_pages = [
            self.__get_players_page(page) for page in range(start_page, end_page + 1)
        ]
        return chain.from_iterable(
            map(parse_players, await asyncio.gather(*result_pages))
        )

    async def __get_ranked_maps_page(self, page: int):
        return await self.__request(url.ranked_maps(page))

    async def get_ranked_maps(self):
        first_page = await self.__get_ranked_maps_page(1)
        total_pages = get_total_pages(first_page["metadata"])
        other_pages = [
            self.__get_ranked_maps_page(page) for page in range(2, total_pages + 1)
        ]
        return chain.from_iterable(
            map(parse_ranked_maps, [first_page] + await asyncio.gather(*other_pages))
        )

    async def get_player_info(self, player_id: str):
        return await self.__request(url.player_info(player_id))
