import logging

from sqlalchemy.exc import SQLAlchemyError

from src.climbsaber.util import DBController


class ScoreSaberStorage:
    def __init__(self, db_controller: DBController):
        self.db_controller = db_controller
        self.validate_tables_exists()

    def validate_tables_exists(self):
        logging.info("Check db tables")
        logging.info("Check songs table")
        try:
            self.db_controller.get_data("select * from songs limit 1")
        except SQLAlchemyError as e:
            logging.warning(f"{e}. Table songs doesn't exists. Creating...")
            self.create_songs_table()
        logging.info("Check players table")
        try:
            self.db_controller.get_data("select * from players limit 1")
        except SQLAlchemyError as e:
            logging.warning(f"{e}. Table players doesn't exists. Creating...")
            self.create_players_table()
        logging.info("Check plays table")
        try:
            self.db_controller.get_data("select * from plays limit 1")
        except SQLAlchemyError as e:
            logging.warning(f"{e}. Table plays doesn't exists. Creating...")
            self.create_plays_table()

    def create_players_table(self):
        create_table_sql = """
            CREATE TABLE "players" (
                "id"	TEXT NOT NULL UNIQUE,
                "name"	TEXT,
                "profilePicture"	TEXT,
                "country"	TEXT,
                "pp"	NUMERIC,
                "rank"	INTEGER,
                "countryRank"	INTEGER,
                "rankedPlayCount"   INTEGER
            )
        """
        self.db_controller.execute(create_table_sql)

    def create_plays_table(self):
        create_table_sql = """
            CREATE TABLE "plays" (
                "id"	INTEGER,
                "rank"	INTEGER,
                "baseScore"	INTEGER,
                "modifiedScore"	INTEGER,
                "pp"	NUMERIC,
                "weight"	NUMERIC,
                "modifiers"	TEXT,
                "multiplier"	NUMERIC,
                "badCuts"	INTEGER,
                "missedNotes"	INTEGER,
                "hmd"	INTEGER,
                "timeSet"	TEXT,
                "leaderboardId"	INTEGER,
                "playerId"	TEXT
            )
        """
        self.db_controller.execute(create_table_sql)

    def create_songs_table(self):
        create_table_sql = """
            CREATE TABLE "songs" (
                "id"	INTEGER NOT NULL UNIQUE,
                "songHash"	TEXT,
                "songName"	TEXT,
                "songSubName"	TEXT,
                "songAuthorName"	TEXT,
                "levelAuthorName"	TEXT,
                "diff"	INTEGER,
                "stars"	REAL,
                "plays"	INTEGER,
                "dailyPlays"	INTEGER,
                "coverImage"	TEXT,
                "rankedDate"	TEXT,
                "maxScore"	INTEGER
            )
        """
        self.db_controller.execute(create_table_sql)

    def insert_songs(self, songs):
        for song in songs:
            try:
                query = f"""INSERT INTO songs (id, "songHash", "songName", "songSubName", "songAuthorName", "levelAuthorName", 
                diff, stars, plays, "dailyPlays", "coverImage", "rankedDate", "maxScore")
                VALUES ({song["id"]}, '{song["songHash"]}', '{song["songName"].replace("'", "")}', 
                '{song["songSubName"].replace("'", "")}', '{song["songAuthorName"].replace("'", "")}', 
                '{song["levelAuthorName"].replace("'", "")}', 
                {song["difficulty"]["difficulty"]}, {song["stars"]}, {song["plays"]}, {song["dailyPlays"]}, 
                '{song["coverImage"]}', '{song["rankedDate"]}', {song["maxScore"]});"""
                self.db_controller.execute(query)
            except Exception as e:
                logging.error(f"""Error while processing query: "{song}" {e}""")

    def insert_players(self, players):
        for player in players:
            try:
                query = f"""INSERT INTO players (id, name, "profilePicture", country, pp, rank, 
                "countryRank", "rankedPlayCount") VALUES (
                '{player["id"]}', '{player["name"].replace("'", "")}', '{player["profilePicture"]}',
                '{player["country"]}', {player["pp"]}, {player["rank"]}, {player["countryRank"]}, 
                 {player["scoreStats"]["rankedPlayCount"]});"""
                self.db_controller.execute(query)
            except Exception as e:
                logging.error(f"""Error while processing query: "{player}" {e}""")

    def insert_plays(self, plays):
        for record in plays:
            try:
                query = f"""INSERT INTO plays (id, rank, "baseScore", "modifiedScore", pp, weight, modifiers, 
                multiplier, "badCuts", "missedNotes", hmd, "timeSet", "leaderboardId", "playerId")
                    VALUES (
                    {record["id"]}, {record["rank"]}, {record["baseScore"]}, {record["modifiedScore"]},
                    {record["pp"]}, {record["weight"]},' {record["modifiers"]}', {record["multiplier"]}, {record["badCuts"]}, 
                    {record["missedNotes"]}, {record["hmd"]}, '{record["timeSet"]}', {record["leaderboardId"]}, {record["playerId"]});"""
                self.db_controller.execute(query)
            except Exception as e:
                logging.error(f"""Error while processing query: "{record}" {e}""")

    def get_players(self):
        return list(
            self.db_controller.get_data(
                """SELECT id, "rankedPlayCount" from players;"""
            ).itertuples()
        )
