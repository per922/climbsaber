import logging
import pickle

import pandas as pd
import scipy.sparse as sparse
from implicit.als import AlternatingLeastSquares

path_to_songs = "data/processed/data_song_full.pkl"
path_to_train = "data/processed/train_data.pkl"
path_out = "models/implicit.pickle"

params = {"factors": 64, "regularization": 0.05}

data_songs = pd.read_pickle(path_to_songs)
cols_to_drop = ["uploaded"]
data = pd.read_pickle(path_to_train).drop(columns=cols_to_drop)
# build data in sparse matrix representation for implicit
train_data = (
    pd.pivot_table(
        data=data, index="leaderboardId", columns="playerId", values="accuracy"
    )
    / 100
)
leaderboard_player = sparse.csr_matrix(train_data.fillna(0))
songs = train_data.index
players = train_data.columns
# get the transpose since the most of the functions in implicit expect (user, item)
# sparse matrices instead of (item, user)
player_leaderboard = leaderboard_player.T.tocsr()
model = AlternatingLeastSquares(
    factors=params["factors"], regularization=params["regularization"]
)
model.fit(player_leaderboard)
model.meta_players = players
model.meta_songs = songs
model.meta_player_leaderboard = player_leaderboard
logging.info("Pickle implicit")
with open(path_out, "wb") as f:
    pickle.dump(model, f)
