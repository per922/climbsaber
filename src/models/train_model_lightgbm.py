import logging
import os

import click
import lightgbm as lgbm
import mlflow
import numpy as np
import pandas as pd
from dotenv import load_dotenv
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.model_selection import train_test_split
from tqdm.auto import tqdm

logging.basicConfig(
    format="[%(asctime)s] (%(levelname)s): %(message)s", level=logging.INFO
)
params = {
    "RANDOM_STATE": 42,
    "TEST_SIZE": 0.2,
    "TARGET_NAME": "accuracy",
    "metric": "mae",
}
load_dotenv()
tqdm.pandas()
np.random.seed(params["RANDOM_STATE"])

remote_server_uri = os.getenv("MLFLOW_TRACKING_URI")
mlflow.set_tracking_uri(remote_server_uri)
mlflow.set_experiment("lightgbm_train")
mlflow.log_params(params)
mlflow.lightgbm.autolog()


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
def train(input_path: str):
    """
    Train the model and log params, metrics and artifacts in MLflow
    :param input_path: train (for [0]) and test (for [1]) dataframes
    :return:
    """

    mlflow.get_artifact_uri()
    logging.info(mlflow.get_artifact_uri())
    cols_to_drop = ["uploaded", "leaderboardId", "playerId", "hmd"]
    data = pd.read_pickle(input_path).drop(columns=cols_to_drop).astype(float)
    train_data, test_data = train_test_split(
        data, test_size=params["TEST_SIZE"], random_state=params["RANDOM_STATE"]
    )
    logging.info(
        f"Data splitted. Parts sizes: train_data = {train_data.shape}, test_data = {test_data.shape}"
    )
    mlflow.log_metrics(
        {"train_rows_count": train_data.shape[0], "test_rows_count": test_data.shape[0]}
    )
    best_params = {
        "n_estimators": 5000,
        "learning_rate": 0.04534004109975924,
        "num_leaves": 1240,
        "max_depth": 9,
        "min_data_in_leaf": 800,
        "lambda_l1": 70,
        "lambda_l2": 70,
        "min_gain_to_split": 12.675416978931597,
        "bagging_fraction": 0.7,
        "bagging_freq": 1,
        "feature_fraction": 0.5,
        "objective": "regression",
        "alpha": 0.4,
    }
    hyper_params = best_params
    hyper_params["n_estimators"] = hyper_params["n_estimators"] * 2
    mlflow.log_params(hyper_params)

    gbm = lgbm.LGBMRegressor(
        task="train",
        boosting_type="gbdt",
        metric=["l1", "l2"],
        verbose=-1,
        **hyper_params,
    )
    gbm.fit(
        X=train_data.drop(columns=[params["TARGET_NAME"]]),
        y=train_data[params["TARGET_NAME"]],
        eval_set=[
            (
                test_data.drop(columns=[params["TARGET_NAME"]]),
                test_data[params["TARGET_NAME"]],
            )
        ],
        eval_metric=params["metric"],
        early_stopping_rounds=1000,
    )
    y_pred = gbm.predict(
        test_data.drop(columns=[params["TARGET_NAME"]]), num_iteration=gbm.best_iteration_
    )
    score = dict(
        model_mae=mean_absolute_error(test_data[params["TARGET_NAME"]], y_pred),
        model_rmse=mean_squared_error(test_data[params["TARGET_NAME"]], y_pred),
    )
    mlflow.log_metrics(score)
    signature = mlflow.lightgbm.infer_signature(
        train_data.drop(columns=[params["TARGET_NAME"]]), y_pred
    )
    mlflow.lightgbm.log_model(
        lgb_model=gbm,
        artifact_path="model",
        registered_model_name="climbsaber_lgbm",
        signature=signature,
    )


if __name__ == "__main__":
    train()
