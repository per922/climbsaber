import logging
import pickle

import pandas as pd

path_to_songs = "data/processed/data_song_full.pkl"
path_to_model = "models/implicit.pickle"


class ImplicitPredictor:
    def __init__(self, player_id, model_path, songs_path):
        self.player_id = int(player_id)
        self.model = self.__load_model(model_path)
        self.songs = pd.read_pickle(songs_path)

    @staticmethod
    def __load_model(model_path):
        """
        :param model_path:
        :return:
        """
        logging.info(f"Loading model: {model_path}")
        with open(model_path, "rb") as file:
            return pickle.load(file)

    def recommend_player(self, count=10, filter_played=True) -> pd.DataFrame:
        if self.player_id not in self.model.meta_players:
            return pd.DataFrame()
        userid = self.model.meta_players.to_list().index(self.player_id)
        ids, scores = self.model.recommend(
            userid,
            self.model.meta_player_leaderboard[userid],
            N=count,
            filter_already_liked_items=filter_played,
        )
        return pd.DataFrame({"songs": self.model.meta_songs[ids], "score": scores})

    def similar_maps(self, song: int, count=10) -> pd.DataFrame:
        if song not in self.model.meta_songs:
            return pd.DataFrame()
        ids, scores = self.model.similar_items(
            self.model.meta_songs.to_list().index(song)
        )
        return pd.DataFrame({"songs": self.model.meta_songs[ids], "score": scores})

    def predict(self) -> pd.DataFrame:
        if self.player_id not in self.model.meta_players:
            return pd.DataFrame()
        result = []
        userid = self.model.meta_players.to_list().index(self.player_id)
        played_maps = (
            self.songs[
                self.songs["id"].isin(
                    self.model.meta_songs[
                        self.model.meta_player_leaderboard[userid].indices
                    ]
                )
            ]
            .sort_values("stars", ascending=False)
            .iloc[:10]
        )
        result.append(self.recommend_player())
        for song in played_maps["id"].to_list():
            result.append(self.similar_maps(song))
        result = pd.concat(result, axis=0).sort_values("score", ascending=False)
        return self.songs[
            (self.songs["id"].isin(result["songs"]))
            & (self.songs["stars"] < played_maps["stars"].median())
        ]


if __name__ == "__main__":
    PLAYER_ID = "76561198001658298"
    predictor = ImplicitPredictor(PLAYER_ID, path_to_model, path_to_songs)
    res = predictor.predict()
    print(res[["songName", "stars"]])
