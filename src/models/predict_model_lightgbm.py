import logging

import mlflow
import pandas as pd
from dotenv import load_dotenv

from src.climbsaber.scoresaber import ScoreSaber


def predict(player_id):
    """
    Predict player accuracy
    :param player_id:
    :return:
    """
    logging.info("Loading model")
    model_name = "climbsaber_lgbm"
    stage = "Production"
    model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{stage}")
    logging.info(f"Loaded {model.metadata}")

    scoresaber = ScoreSaber()
    logging.info(f"Starting prediction for player: {player_id}")
    data_playes = pd.DataFrame(scoresaber.get_history(player_id, max_pages=10))
    data_playes["playerId"] = player_id
    data_playes = data_playes[
        [
            "leaderboardId",
            "playerId",
            "modifiedScore",
            "badCuts",
            "missedNotes",
            "rank",
            "pp",
            "hmd",
            "weight",
        ]
    ]
    songs = pd.read_pickle("data/processed/data_song_full.pkl")
    data_songs = songs.merge(
        data_playes, how="left", left_on="id", right_on="leaderboardId"
    )
    data_songs["accuracy"] = data_songs["modifiedScore"] / data_songs["maxScore"] * 100
    song_info_cols = songs.attrs["song_info_cols"]
    cols = [
        "pp",
        "accuracy",
        "npm",
        "stars",
        "duration",
        "rating",
        "obstacles",
    ] + song_info_cols
    data_player_agg = data_songs[cols].agg(["mean", "max", "min"]).unstack()
    data_player_agg.index = [f"player_{'_'.join(col)}" for col in data_player_agg.index]
    data_player_agg = data_player_agg.to_frame(player_id).T
    for col in data_player_agg.columns:
        data_songs[col] = data_player_agg[col].iloc[0]
    cols2 = [
        col
        for col in data_songs.columns
        if col.startswith("player_") and "pp" not in col and "accuracy" not in col
    ]
    for col in cols2:
        data_songs[f"diff_{col}"] = (
            data_player_agg[col].iloc[0] - data_songs["_".join(col.split("_")[1:-1])]
        )
    data_songs["stars_bin"] = pd.cut(
        data_songs["stars"], bins=[0, 2, 4, 6, 8, 10, 99]
    ).cat.codes
    data = data_songs  # [model_cols].copy()
    # cols dtype fix
    for col in ["plays", "dailyPlays", "duration", "downvotes", "upvotes"]:
        data[col] = data[col].astype(float)
    return model.predict(data)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    load_dotenv()
    print(predict("76561198001658298"))
