import asyncio
import logging

from aiohttp import ClientSession, TCPConnector


class BeatSaverAsync:
    async def __aenter__(self) -> "BeatSaverAsync":
        conn = TCPConnector(limit=50)
        self.session = ClientSession("https://eu.cdn.beatsaver.com/", connector=conn)
        return self

    async def __aexit__(self, exc_t, exc_v, exc_tb) -> None:
        await self.session.close()

    async def __request(self, url) -> bytes:
        async with self.session.get(url) as response:
            return await response.read()

    async def get_song(self, song_hash: str) -> bytes:
        attempt = 1
        while attempt < 5:
            try:
                return await self.__request(f"/{song_hash}.zip")
            except Exception as ex:
                logging.warning(
                    f"{song_hash}: attempt {attempt} Exception: {type(ex).__name__}"
                )
                await asyncio.sleep(attempt)
                attempt += 1
