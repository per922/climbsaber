import json
import os

import pandas as pd
from dotenv import load_dotenv
from sqlalchemy import create_engine, text


class DBController:
    """POSTGRESQL manipulator :D"""

    def __init__(
        self, db_host: str, db_port: str, db_name: str, db_user: str, db_pass: str
    ):
        """
        :param db_host:
        :param db_port:
        :param db_name:
        :param db_user:
        :param db_pass:
        """
        self.connection = create_engine(
            f"postgresql+psycopg2://{db_user}:{db_pass}@{db_host}:{db_port}/{db_name}"
        )

    def execute(self, query: str) -> None:
        """Execute sql query
        :param query:
        """
        self.connection.execute(text(query))

    def get_data(self, query: str) -> pd.DataFrame:
        """Return query response in pandas dataframe with columns
        :param query:
        :return:
        """
        return pd.read_sql(text(query), self.connection)


def unify_diff(diff: str) -> int:
    """
    Convert diff string to int representation
    :param diff:
    :return:
    """
    diff_map = {
        9: "_ExpertPlus_SoloStandard",
        7: "_Expert_SoloStandard",
        5: "_Hard_SoloStandard",
        3: "_Normal_SoloStandard",
        1: "_Easy_SoloStandard",
    }
    diff_map_reverse = dict((v, k) for k, v in diff_map.items())
    if isinstance(diff, int):
        return diff
    if "SoloStandard" not in diff:
        diff = f"_{diff}_SoloStandard"
    return diff_map_reverse.get(diff, -99)


def diff_to_text(diff: int) -> str:
    """
    Convert int representation to string
    :param diff:
    :return:
    """
    diff_map = {9: "ExpertPlus", 7: "Expert", 5: "Hard", 3: "Normal", 1: "Easy"}
    return diff_map.get(int(diff), "Unknown")


def song_to_playlist_repr(song: pd.Series) -> dict:
    """
    Convert series with song to dict in playlist format
    :param song:
    :return:
    """
    return {
        "hash": song["songHash"],
        "songName": song["songName"],
        "difficulties": [{"characteristic": "Standard", "name": song["diff"]}],
    }


def generate_playlist(
    data: pd.DataFrame, title: str, author: str, image: str, player_id: str
):
    """
    Generate bplist for Beat Saber.
    :param data:
    :param title:
    :param author:
    :param image:
    :param player_id:
    :return:
    """
    load_dotenv()
    api_url = os.getenv("CLIMBSABER_API_URL")
    playlist = {
        "playlistTitle": title,
        "playlistAuthor": author,
        "customData": {"syncURL": f"{api_url}/playlists?player_id={player_id}"},
        "image": image,
        "songs": data.apply(song_to_playlist_repr, axis=1).to_list(),
    }
    with open(f"files/{title.lower()}_{player_id}.bplist", "w", encoding="UTF-8") as file:
        json.dump(playlist, file)
