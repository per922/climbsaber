import logging
import warnings

import numpy as np
import pandas as pd
from dotenv import load_dotenv
from tqdm.auto import tqdm

from src.climbsaber.scoresaber import ScoreSaber
from src.climbsaber.util import unify_diff
from src.config import SONGS_FULL_DATA_PATH
from src.models.predict_model_lightgbm import predict as predict_lgbm

tqdm.pandas()
warnings.filterwarnings("always")
warnings.simplefilter("ignore")
logging.basicConfig(
    format="[%(asctime)s] (%(levelname)s): %(message)s", level=logging.INFO
)
load_dotenv()


class Predictor:
    """Class for prediction how much performance points player will gain and other stats"""

    def __init__(self, songs_full_data_path):
        self.songs = self.__load_songs(songs_full_data_path)
        self.scoresaber = ScoreSaber()
        self.pp_curve = [
            (0, 0),
            (0.6, 0.25),
            (0.65, 0.29),
            (0.7, 0.34),
            (0.75, 0.40),
            (0.8, 0.47),
            (0.825, 0.51),
            (0.85, 0.57),
            (0.875, 0.655),
            (0.9, 0.75),
            (0.91, 0.79),
            (0.92, 0.835),
            (0.93, 0.885),
            (0.94, 0.94),
            (0.95, 1),
            (0.955, 1.045),
            (0.96, 1.11),
            (0.965, 1.20),
            (0.97, 1.31),
            (0.9725, 1.37),
            (0.975, 1.45),
            (0.9775, 1.57),
            (0.98, 1.71),
            (0.9825, 1.88),
            (0.985, 2.1),
            (0.9875, 2.38),
            (0.99, 2.73),
            (0.9925, 3.17),
            (0.995, 3.76),
            (0.9975, 4.7),
            (0.999, 5.8),
            (1, 7),
            (1.001, 7),
        ]

    @staticmethod
    def __load_songs(songs_path):
        """
        :param songs_path:
        :return:
        """
        logging.info(f"Loading songs data: {songs_path}")
        return pd.read_pickle(songs_path)

    @staticmethod
    def predict_neighbours_mean(player_id, rank_range: int):
        """
        predict accuracy using rank neighbours scores
        :param player_id:
        :param rank_range:
        :return:
        """
        data = pd.read_pickle("data/processed/train_data.pkl")
        data_players = pd.read_pickle("data/interim/players.pkl")
        player = data_players[data_players["id"] == player_id]
        if not len(player):
            return {}
        player_rank = player["rank"].iloc[0]
        neighbours_ids = pd.to_numeric(
            data_players[
                data_players["rank"].between(
                    player_rank - rank_range, player_rank + rank_range
                )
            ]["id"]
        )
        leaderboard_mean = (
            data[data["playerId"].isin(neighbours_ids)]
            .groupby("leaderboardId")["accuracy"]
            .mean()
        )
        return leaderboard_mean.to_dict()

    def __get_raw_pp(self):
        """
        :return:
        """
        logging.info("Getting raw pp")
        raw_pp = (
            pd.DataFrame(self.scoresaber.get_raw_pp()).unstack().to_frame("pp").dropna()
        )
        raw_pp = raw_pp.reset_index().rename(
            columns={"level_0": "songHash", "level_1": "diff", "pp": "raw_pp"}
        )
        raw_pp["songHash"] = raw_pp["songHash"].str.upper()
        raw_pp["diff"] = raw_pp["diff"].apply(unify_diff)
        return raw_pp

    @staticmethod
    def calc_lerp(x1: float, y1: float, x2: float, y2: float, x3: float) -> float:
        """
        lerp for interpolate pp curve
        :param x1:
        :param y1:
        :param x2:
        :param y2:
        :param x3:
        :return:
        """
        m = (y2 - y1) / (x2 - x1)
        return m * (x3 - x1) + y1

    def pp_perc(self, accuracy: float) -> float:
        """
        calculate multiplication for raw pp
        :param accuracy:
        :return:
        """
        if accuracy >= 1.14:
            return 1.25
        if accuracy <= 0:
            return 0
        i = -1
        for score, _ in self.pp_curve:
            if score > accuracy:
                break
            i += 1
        lower_score = self.pp_curve[i][0]
        higher_score = self.pp_curve[i + 1][0]
        lower_given = self.pp_curve[i][1]
        higher_given = self.pp_curve[i + 1][1]
        return self.calc_lerp(
            lower_score, lower_given, higher_score, higher_given, accuracy
        )

    def calculate_pp(self, raw_pp: float, accuracy: float) -> float:
        """
        return how much pp you will receive with given accuracy and basic pp for song
        :param raw_pp:
        :param accuracy:
        :return:
        """
        return raw_pp * self.pp_perc(accuracy)

    @staticmethod
    def calculate_global_change(
        history, score_row, weight_curve, new_pp_col="potencial_pp"
    ):
        """
        return much pp you will receive with given accuracy and basic pp for song taking into account song weight curve
        :param history:
        :param score_row:
        :param weight_curve:
        :param new_pp_col:
        :return:
        """
        t_history = history.copy().set_index("leaderboardId")
        t_history.loc[score_row["leaderboardId"], "pp"] = score_row[new_pp_col]
        t_history = t_history.sort_values("pp", ascending=False)
        t_score = 0
        for score, weight in zip(t_history["pp"].values, weight_curve[: len(t_history)]):
            t_score += score * weight
        return t_score

    @staticmethod
    def mix_scores(
        prediction_1: pd.Series, prediction_2: pd.Series, weights: list
    ) -> list:
        """
        calculate average prediction based on two with weights and empty predict protect
        :param prediction_1:
        :param prediction_2:
        :param weights:
        :return:
        """
        result = []
        for i, j in zip(prediction_1, prediction_2):
            if i == 0:
                i = j
            if j == 0:
                j = i
            result.append(np.average([i, j], weights=weights))
        return result

    def compare_with_player(self, player: str, player2: str) -> pd.Series:
        """
        compare history of two player
        :param player:
        :param player2:
        :return:
        """
        player_history = pd.DataFrame(self.scoresaber.get_history(player, max_pages=80))
        player2_history = pd.DataFrame(self.scoresaber.get_history(player2, max_pages=80))
        player2_history = player2_history.rename(
            columns={"pp": "pp_opponent", "modifiedScore": "modifiedScore_opponent"}
        )
        data = player2_history.merge(
            player_history[["leaderboardId", "pp"]], how="left", on="leaderboardId"
        ).fillna(0)
        weight_curve = player_history["weight"].to_list() + [0] * (
            len(data) - len(player_history)
        )
        global_score = 0
        for score, weight in zip(
            player_history["pp"].values, weight_curve[: len(player_history)]
        ):
            global_score += score * weight
        data["new_global"] = data.apply(
            lambda x: self.calculate_global_change(
                player_history, x, weight_curve, "pp_opponent"
            ),
            axis=1,
        )
        data["pp_change_opponent"] = data["new_global"] - global_score
        data["id"] = pd.to_numeric(data["leaderboardId"])
        return data.set_index("id")[
            ["pp_change_opponent", "pp_opponent", "modifiedScore_opponent"]
        ]

    def prediction_pp(self, player_id: str, player_id2=None) -> pd.DataFrame:
        """
        Main prediction method. Combine all models and predict pp growth
        :param player_id:
        :param player_id2:
        :return:
        """
        data = self.songs
        lgbm_predict = predict_lgbm(player_id)
        neighbours_score = self.predict_neighbours_mean(player_id, 50)
        neighbours_predict = data["id"].apply(lambda x: neighbours_score.get(x, 0))
        data["predict"] = self.mix_scores(
            lgbm_predict, neighbours_predict, weights=[1, 0]
        )
        data = data.merge(self.__get_raw_pp(), how="left", on=["songHash", "diff"])
        data["potencial_pp"] = data[["raw_pp", "predict"]].apply(
            lambda x: self.calculate_pp(x["raw_pp"], x["predict"] / 100), axis=1
        )
        logging.info(f"Loading player {player_id} history")
        player_history = pd.DataFrame(
            self.scoresaber.get_history(player_id, max_pages=80)
        )
        data = data.merge(
            player_history[["leaderboardId", "modifiedScore", "pp"]],
            how="left",
            left_on=["id"],
            right_on=["leaderboardId"],
        )
        logging.info(f"Calculate player {player_id} aggregation metrics")
        data["accuracy"] = data["modifiedScore"] / data["maxScore"] * 100
        cols = ["stars", "pp", "accuracy", "npm"]
        data_player_agg = data[cols].agg(["mean", "max", "min"]).unstack()
        data_player_agg.index = [
            f"player_{'_'.join(col)}" for col in data_player_agg.index
        ]
        data_player_agg = data_player_agg.to_frame(player_id).T
        for col in data_player_agg.columns:
            data[col] = data_player_agg[col].iloc[0]
        logging.info(f"Calculate pp change for player {player_id}")
        weight_curve = player_history["weight"].to_list() + [0] * (
            len(data) - len(player_history)
        )
        global_score = 0
        for score, weight in zip(
            player_history["pp"].values, weight_curve[: len(player_history)]
        ):
            global_score += score * weight
        data["new_global"] = data.apply(
            lambda x: self.calculate_global_change(player_history, x, weight_curve),
            axis=1,
        )
        data["pp_change"] = data["new_global"] - global_score
        if player_id2:
            opponent = self.compare_with_player(player_id, player_id2)
            data = data.merge(opponent, how="left", left_on=["id"], right_index=True)
            data["accuracy_opponent"] = (
                data["modifiedScore_opponent"] / data["maxScore"] * 100
            )
        return data


if __name__ == "__main__":
    PLAYER_ID = "76561198001658298"  # 76561198001658298 76561198080503576
    predictor = Predictor(
        songs_full_data_path=SONGS_FULL_DATA_PATH,
    )
    res = predictor.prediction_pp(PLAYER_ID)
    print(res)
