import asyncio
import logging
import sys
from itertools import chain

import requests
from requests.adapters import HTTPAdapter, Retry

from src.scoresaber.api_client import ScoreSaberAsync
from src.scoresaber.storage import ScoreSaberStorage


class ScoreSaber:
    """api wrapper for ScoreSaber.com"""

    def __init__(self):
        if sys.platform == "win32":
            asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
        self.requests = self.__create_request_session()

    @staticmethod
    def __create_request_session() -> requests.Session:
        session = requests.Session()
        retry_strategy = Retry(
            total=5,
            backoff_factor=2,
            status_forcelist=[429, 500, 502, 503, 504],
            # allowed_methods=["HEAD", "GET", "OPTIONS"]
        )
        session.mount("https://scoresaber.com/", HTTPAdapter(max_retries=retry_strategy))
        return session

    @staticmethod
    async def __get_player_info_async(player_id: str) -> dict:
        async with ScoreSaberAsync() as scoresaber:
            return await scoresaber.get_player_info(player_id)

    def get_player_info(self, player_id: str) -> dict:
        """
        :param: player_id: player id from scoresaber website
        :return: player information: rank, pp, country, pic and etc.
        """
        return asyncio.run(self.__get_player_info_async(player_id))

    @staticmethod
    async def __get_history_async(player_id: str, max_pages: int = 5) -> dict:
        async with ScoreSaberAsync() as scoresaber:
            return await scoresaber.get_scores(player_id, max_pages)

    def get_history(self, user_id: str, max_pages: int = 5) -> list:
        """return player play history (only ranked)
        :param user_id:
        :param max_pages:
        :return:
        """
        return list(asyncio.run(self.__get_history_async(user_id, max_pages)))

    def get_raw_pp(self) -> dict:
        """
        :return: how much you can gain performance points from map
        """
        url = "https://cdn.pulselane.dev/raw_pp.json"
        try:
            response = self.requests.get(url, allow_redirects=True)
            return response.json()
        except requests.HTTPError as error:
            logging.error(f"Can't download raw pp data\n{error}")
        return dict()


class ScoreSaberCollector:
    """Scrap ScoreSaber.com data to sqlite db"""

    def __init__(self, storage: ScoreSaberStorage, scoresaber: ScoreSaberAsync):
        """
        :param db_instance: DBController from util module
        :param ss_instance: ScoreSaber class from this module
        """
        self.storage = storage
        self.scoresaber = scoresaber

    async def collect_songs(self) -> None:
        logging.info("start collecting songs!")
        songs = list(await self.scoresaber.get_ranked_maps())
        self.storage.insert_songs(songs)
        logging.info(f"{len(songs)} songs collected!")

    async def collect_top_players(self, start_page=1, end_page=2) -> None:
        logging.info("start collecting players!")
        players = list(await self.scoresaber.get_players(start_page, end_page))
        self.storage.insert_players(players)
        logging.info(f"{len(players)} players collected!")

    async def collect_player_history(self, player, num_pages):
        pages = min(num_pages, 1 + player.rankedPlayCount // 100)
        return await self.scoresaber.get_scores(player.id, pages)

    async def collect_players_history(self, num_pages=10, skip_first=0) -> None:
        logging.info("start collecting players history!")
        players = self.storage.get_players()
        history_requests = [
            self.collect_player_history(player, num_pages)
            for player in players[skip_first:]
        ]
        plays = list(chain.from_iterable(await asyncio.gather(*history_requests)))
        self.storage.insert_plays(plays)
        logging.info(f"{len(plays)} plays loaded for {len(players)} players!")
