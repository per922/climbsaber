import json
import logging
import zipfile
from io import BytesIO

import pandas as pd
import requests
from requests.adapters import HTTPAdapter, Retry

from src.climbsaber.util import unify_diff


class BeatSaver:
    """Class for getting parsed info, do basic processing and download songs from beatsaver.com"""

    def __init__(self):
        self.requests = self.__create_request_session()

    @staticmethod
    def __create_request_session() -> requests.Session:
        session = requests.Session()
        retry_strategy = Retry(
            total=5,
            backoff_factor=2,
            status_forcelist=[429, 500, 502, 503, 504],
            # allowed_methods=["HEAD", "GET", "OPTIONS"]
        )
        session.mount(
            "https://raw.githubusercontent.com/",
            HTTPAdapter(max_retries=retry_strategy),
        )
        return session

    def download_data(self) -> list:
        """
        download already scrapped data from git. Thanks for it to andruzzzhka
        :return: list of songs
        """
        url = "https://raw.githubusercontent.com/andruzzzhka/BeatSaberScrappedData/master/combinedScrappedData.zip"
        try:
            response = self.requests.get(url, allow_redirects=True)
            with zipfile.ZipFile(file=BytesIO(response.content)) as zip_ref:
                return json.loads(zip_ref.read(name="combinedScrappedData.json"))
        except requests.HTTPError as error:
            logging.error(f"Can't download beatsaver scrapped data\n{error}")
            return []

    def process_scrapped_data(self, out_path: str) -> None:
        """
        Basic processing data: json normalization, filter cols and splitting levels from map by diff
        :param out_path: path to save data
        """
        beatsaver = pd.json_normalize(self.download_data())
        beatsaver.columns = [col.lower() for col in beatsaver.columns]
        beatsaver = beatsaver[
            [
                "hash",
                "key",
                "uploaded",
                "duration",
                "chars",
                "downvotes",
                "upvotes",
                "diffs",
            ]
        ].rename(columns={"hash": "songHash"})
        beatsaver["rating"] = (beatsaver["upvotes"] - beatsaver["downvotes"]) / (
            beatsaver["upvotes"] + beatsaver["downvotes"]
        )
        beatsaver["uploaded"] = pd.to_datetime(beatsaver["uploaded"])
        beatsaver["uploaded"] = pd.to_numeric(beatsaver["uploaded"]) // 1000000
        beatsaver["diffs"] = beatsaver["diffs"].apply(
            lambda x: [y for y in x if y["Char"] == "Standard"]
        )
        beatsaver["diff"] = beatsaver["diffs"].copy()
        beatsaver = beatsaver.explode("diff")
        beatsaver_diffs = beatsaver["diff"].apply(pd.Series)
        del beatsaver["diff"], beatsaver_diffs[0]
        beatsaver_diffs.columns = [col.lower() for col in beatsaver_diffs.columns]
        beatsaver_diffs = beatsaver_diffs[beatsaver_diffs["ranked"].fillna(False)]
        beatsaver_diffs["diff"] = beatsaver_diffs["diff"].apply(unify_diff)
        beatsaver = beatsaver.merge(
            beatsaver_diffs, how="left", left_index=True, right_index=True
        )
        beatsaver = beatsaver.drop(
            columns=[
                "char",
                "chars",
                "diffs",
                "rankedupdatetime",
                "njsoffset",
                "requirements",
                "ranked",
                "stars",
            ]
        )
        beatsaver = beatsaver.dropna(axis=0)
        beatsaver["npm"] = beatsaver["notes"] / beatsaver["duration"]
        beatsaver = beatsaver.drop_duplicates(subset=["songHash", "diff"])
        beatsaver.to_pickle(out_path)
