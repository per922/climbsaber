import ast
import os
import warnings
from typing import Dict

import numpy as np
import pandas as pd
from tqdm import tqdm

from src.climbsaber.util import unify_diff

warnings.filterwarnings("always")
warnings.simplefilter("ignore")
tqdm.pandas()


class SongParser:
    """Beat Saber song file parser. Generate features like num blocks in minute and etc"""

    def __init__(self, song_path="data/songs/"):
        self.song_path = song_path

    @staticmethod
    def read_file(file_path) -> dict:
        """
        read .dat map file and returns result as dict
        :param file_path:
        :return:
        """
        return ast.literal_eval(
            open(file_path, encoding="UTF-8")
            .read()
            .replace("null", "None")
            .replace("false", "False")
            .replace("true", "True")
            .replace("\n", "")
        )

    @staticmethod
    def q80(x: pd.Series) -> float:
        """
        :param x: series of float
        :return:
        """
        return x.quantile(0.8)

    @staticmethod
    def q95(x: pd.Series) -> float:
        """
        :param x: series of float
        :return:
        """
        return x.quantile(0.95)

    @staticmethod
    def calc_angle(angle_1: int, angle_2: int) -> int:
        """
        Calculate angel between two blocks direction
        :param angle_1:
        :param angle_2:
        :return: angel
        """
        if np.isnan(angle_1) or np.isnan(angle_2):
            return 0
        return 180 - abs(abs(angle_1 - angle_2) - 180)

    def is_classic_slider(self, pattern: pd.Series) -> str:
        """
        Return is this pattern a slider and if it is then type of it
        :param pattern:
        :return:
        """
        height_change = abs(pattern["_lineLayer"].diff().fillna(0).sum())
        weight_change = abs(pattern["_lineIndex"].diff().fillna(0).sum())
        pattern["note_direction_angle_shift"] = pattern["note_direction_angle"].shift()
        note_angle_change = (
            pattern.apply(
                lambda x: self.calc_angle(
                    x["note_direction_angle"], x["note_direction_angle_shift"]
                ),
                axis=1,
            )
            .fillna(0)
            .abs()
            .sum()
        )
        if note_angle_change > 90:
            return ""
        if height_change == 2 and weight_change <= 1:
            return "classic_vertical"
        if height_change <= 1 and weight_change >= 2:
            return "classic_horizontal"
        if height_change == 2 and weight_change >= 2:
            return "classic_diagonal"
        return ""

    def is_2note_slider(self, pattern: pd.Series):
        """
        Return is this pattern a short slider and if it is then type of it
        :param pattern:
        :return:
        """
        height_change = abs(pattern["_lineLayer"].diff().fillna(0).sum())
        weight_change = abs(pattern["_lineIndex"].diff().fillna(0).sum())
        pattern["note_direction_angle_shift"] = pattern["note_direction_angle"].shift()
        note_angle_change = (
            pattern.apply(
                lambda x: self.calc_angle(
                    x["note_direction_angle"], x["note_direction_angle_shift"]
                ),
                axis=1,
            )
            .fillna(0)
            .abs()
            .sum()
        )
        if note_angle_change > 0:
            return False
        if height_change == 1 and weight_change == 0:
            return "2note_vertical"
        if height_change == 0 and weight_change == 1:
            return "2note_horizontal"
        if height_change == 1 and weight_change == 1:
            return "2note_diagonal"
        if height_change >= 2:
            return "2note_window_vertical"
        if weight_change >= 2:
            return "2note_window_horizontal"
        return False

    @staticmethod
    def is_bad_note(note: pd.Series) -> bool:
        """
        Return True if note direction is cringe. I mean for example red note in right side directed to left.
        :param note:
        :return:
        """
        if (
            note["note_side"] == "right"
            and note["note_color"] == "red"
            and 180 < note["note_direction_angle"] < 360
        ):
            return True
        if (
            note["note_side"] == "left"
            and note["note_color"] == "blue"
            and 0 < note["note_direction_angle"] < 180
        ):
            return True
        return False

    def is_crosshair(self, pattern: pd.Series):
        """
        Return True if note coming in crosshair
        """
        pattern["note_direction_angle_shift"] = pattern["note_direction_angle"].shift()
        note_angle_change = (
            pattern.apply(
                lambda x: self.calc_angle(
                    x["note_direction_angle"], x["note_direction_angle_shift"]
                ),
                axis=1,
            )
            .fillna(0)
            .abs()
            .sum()
        )
        first_note = pattern.iloc[0]
        second_note = pattern.iloc[1]
        return (
            note_angle_change > 135
            and first_note["note_height"] != second_note["note_height"]
            and first_note["note_side"] != second_note["note_side"]
        )

    def is_reset(self, pattern: pd.Series):
        """
        True if two notes for same hand comes in some direction
        :param pattern:
        :return:
        """
        pattern["note_direction_angle_shift"] = pattern["note_direction_angle"].shift()
        note_angle_change = (
            pattern.apply(
                lambda x: self.calc_angle(
                    x["note_direction_angle"], x["note_direction_angle_shift"]
                ),
                axis=1,
            )
            .fillna(0)
            .abs()
            .sum()
        )
        first_note = pattern.iloc[0]
        second_note = pattern.iloc[1]
        return (
            note_angle_change <= 45 and first_note["second"] - second_note["second"] > 0.1
        )

    @staticmethod
    def calc_obstacles_stats(data_obs: pd.DataFrame, bpm: float):
        """
        Calculate walls stats
        :param data_obs:
        :param bpm:
        :return:
        """
        level_stats = pd.Series()
        if len(data_obs):
            data_obs["type"] = data_obs["_type"].map({0: "full", 1: "duck"})
            data_obs["_time"] = data_obs["_duration"] / (bpm / 60)
            data_obs = data_obs.sort_values("_time")
            data_obs["duration"] = data_obs["_duration"] / (bpm / 60)
            data_obs["start_line"] = data_obs.apply(
                lambda x: x["_lineIndex"]
                if x["_width"] > 0
                else x["_lineIndex"] + x["_width"],
                axis=1,
            )
            data_obs["end_line"] = data_obs.apply(
                lambda x: x["_lineIndex"] + (x["_width"] - 1)
                if x["_width"] > 0
                else x["_lineIndex"] + (x["_width"] - 1),
                axis=1,
            )
            for line in range(4):
                data_obs[f"line_{line}"] = data_obs.apply(
                    lambda x: x["start_line"] <= line <= x["end_line"], axis=1
                )
            level_stats["wall_total_duration"] = data_obs["duration"].abs().sum()
            level_stats["wall_half_face"] = (
                (data_obs["line_1"]) | (data_obs["line_2"])
            ).sum()
            level_stats["wall_full_face"] = (
                (data_obs["line_1"]) & (data_obs["line_2"])
            ).sum()
            level_stats["wall_seat"] = (
                (data_obs["line_1"]) & (data_obs["line_2"]) & (data_obs["type"] == "duck")
            ).sum()
            level_stats["wall_seat_duration"] = (
                data_obs[
                    (
                        (data_obs["line_1"])
                        & (data_obs["line_2"])
                        & (data_obs["type"] == "duck")
                    )
                ]["duration"]
                .abs()
                .sum()
            )

        return level_stats

    def calc_notes_stats(self, data_notes: pd.DataFrame, bpm: float) -> pd.DataFrame:
        """
        Stats based on notes
        :param data_notes:
        :param bpm:
        :return:
        """
        level_stats = pd.Series()
        if len(data_notes) == 0:
            return level_stats
        data_notes["second"] = data_notes.get("_time", 0) / (bpm / 60)
        data_notes["second_td"] = pd.to_timedelta(data_notes["second"], unit="s")
        data_notes = data_notes.sort_values("second_td")
        data_notes["note_side"] = data_notes["_lineIndex"].apply(
            lambda x: "left" if x < 2 else "right"
        )
        data_notes["note_height"] = data_notes["_lineLayer"].map(
            {0: "bot", 1: "mid", 2: "high"}
        )
        data_notes["note_color"] = data_notes["_type"].map({0: "red", 1: "blue"})
        data_notes["is_bomb"] = data_notes["_type"] == 3
        data_notes["is_dot_note"] = data_notes["_cutDirection"] == 8
        data_notes["note_direction_angle"] = data_notes["_cutDirection"].map(
            {0: 0, 1: 180, 2: 270, 3: 90, 4: 315, 5: 45, 6: 225, 7: 135}
        )  # 0 - top, 90 - right
        data_notes["pos"] = data_notes.apply(
            lambda x: np.array((x["_lineIndex"], x["_lineLayer"])), axis=1
        )

        # VERTICAL / HORIZONTAL RATE
        vertical = data_notes["note_direction_angle"].isin([0, 180]).sum()
        horizontal = data_notes["note_direction_angle"].isin([90, 270]).sum()
        level_stats["h/v rate"] = horizontal / vertical
        level_stats["hor rate"] = horizontal / len(data_notes)

        # DIAGONAL / VERTICAL + HORIZONTAL
        diagonal = data_notes["note_direction_angle"].isin([45, 135, 225, 315]).sum()
        level_stats["diag/hv rate"] = diagonal / vertical + horizontal

        # SLIDERS
        slider_counter: Dict[str, int] = dict()
        for window in (
            data_notes[~data_notes["is_bomb"]]
            .set_index("second_td")
            .groupby("note_color")
            .rolling("100ms", min_periods=3)
        ):
            if len(window) >= 3:
                for i in range(len(window) - 2):
                    window_3 = window[i : i + 3]
                    slider_type = self.is_classic_slider(window_3)
                    if slider_type:
                        slider_counter[slider_type] = (
                            slider_counter.get(slider_type, 0) + 1
                        )
        for window in (
            data_notes[~data_notes["is_bomb"]]
            .set_index("second_td")
            .groupby("note_color")
            .rolling("100ms", min_periods=2)
        ):
            if len(window) >= 2:
                for i in range(len(window) - 1):
                    window_2 = window[i : i + 2]
                    slider_type = self.is_2note_slider(window_2)
                    if slider_type:
                        slider_counter[slider_type] = (
                            slider_counter.get(slider_type, 0) + 1
                        )
        level_stats = level_stats.append(pd.Series(slider_counter))

        # METRIC PER SEC
        for sec in ["3s", "5s", "10s"]:
            t = (
                data_notes.set_index("second_td")
                .rolling(sec, min_periods=1)["note_side"]
                .count()
                .dropna()
                .agg(["mean", "max", self.q80, self.q95])
            )
            t.index = [f"{sec}_{c}" for c in t.index]
            level_stats = level_stats.append(t)

        # METRIC PER HAND PER SEC
        for sec in ["3s", "5s", "10s"]:
            t = (
                data_notes.set_index("second_td")
                .groupby("note_color")
                .rolling(sec, min_periods=1)["note_side"]
                .count()
                .dropna()
                .reset_index()
                .groupby(["note_color"])["note_side"]
                .agg(["mean", "max", self.q80, self.q95])
                .unstack()
            )
            t.index = [f"{sec}_per_hand_{c[1]}_{c[0]}" for c in t.index]
            level_stats = level_stats.append(t)

        # JUMPS
        jump_counter: Dict[int, int] = dict()
        for color in ("blue", "red"):
            data_notes_color = data_notes[data_notes["note_color"] == color]
            for i in range(len(data_notes_color) - 1):
                pattern = data_notes_color.iloc[i : i + 2]
                first_note = pattern.iloc[0]
                second_note = pattern.iloc[1]
                angle = self.calc_angle(
                    first_note["note_direction_angle"],
                    second_note["note_direction_angle"],
                )
                distance = round(np.linalg.norm(first_note["pos"] - second_note["pos"]))
                if angle > 90:
                    jump_counter[distance] = jump_counter.get(distance, 0) + 1
        t = pd.Series(jump_counter)
        t = t / t.sum() * 100
        t.index = [f"distance_{int(i)}" for i in t.index]
        level_stats = level_stats.append(t)

        # jump_counter_color: Dict[str, list] = dict()
        # for sec in ["3s", "5s", "10s"]:
        #     for color in ("blue", "red"):
        #         data_notes_color = data_notes[data_notes["note_color"] == color]
        #         jump_counter_color[f"distance_{sec}_{color}"] = []
        #         for window in data_notes_color.set_index("second_td").rolling(sec, min_periods=5):
        #             if len(window) > 5:
        #                 window["pos_shift"] = window["pos"].shift()
        #                 distance = window.apply(lambda x: np.linalg.norm(x["pos"] - x["pos_shift"]), axis=1).sum()
        #                 jump_counter_color[f"distance_{sec}_{color}"].append(distance)

        t = (
            pd.Series(jump_counter)
            .explode()
            .reset_index()
            .rename(columns={0: "distance"})
        )
        t["distance"] = pd.to_numeric(t["distance"])
        t = (
            t.groupby("index")["distance"]
            .agg(["mean", "max", self.q80, self.q95])
            .unstack()
        )
        t.index = [f"distance_{int(c[1])}_{c[0]}" for c in t.index]
        level_stats = level_stats.append(t)

        # BAD NOTES
        level_stats["bad_notes_count"] = data_notes.apply(self.is_bad_note, axis=1).sum()

        # BOMBS
        level_stats["bomb_count"] = data_notes["is_bomb"].sum()

        # CROSSHAIRS
        crosshair_counter = 0
        crosshair_strike = 0
        prev_crosshair = False
        for window in (
            data_notes[~data_notes["is_bomb"]]
            .set_index("second_td")
            .groupby("note_color")
            .rolling("300ms", min_periods=3)
        ):
            if len(window) >= 2:
                for i in range(len(window) - 1):
                    window_2 = window[i : i + 2]
                    crosshair = self.is_crosshair(window_2)
                    if prev_crosshair and crosshair:
                        crosshair_strike += 1
                    prev_crosshair = crosshair
                    if crosshair:
                        crosshair_counter += 1
        level_stats["crosshair_count"] = crosshair_counter
        level_stats["crosshair_strike_count"] = crosshair_strike

        # RESETS
        reset_counter = 0
        for window in (
            data_notes[~data_notes["is_bomb"]]
            .set_index("second_td")
            .groupby("note_color")
            .rolling("300ms", min_periods=3)
        ):
            if len(window) >= 2:
                for i in range(len(window) - 1):
                    window_2 = window[i : i + 2]
                    reset = self.is_reset(window_2)
                    if reset:
                        reset_counter += 1
        level_stats["reset_count"] = reset_counter
        return level_stats

    def parse_song(self) -> pd.DataFrame:
        """
        Main method for parse song
        :param song_folder: path to folder with song
        :return:
        """
        data_song_stats = pd.DataFrame()
        file_path = f"{self.song_path}/info.dat"
        if not os.path.exists(file_path):
            file_path = f"{self.song_path}/Info.dat"
            if not os.path.exists(file_path):
                print(f"ERROR SONG INFO NOT FOUND {self.song_path}")
                return data_song_stats
        data_song = self.read_file(file_path)
        bpm = data_song["_beatsPerMinute"]
        song_hash = self.song_path.split("\\")[1].split("_")[0]
        diffs = [
            diffs
            for diffs in data_song.get("_difficultyBeatmapSets", [])
            if diffs.get("_beatmapCharacteristicName", "") == "Standard"
        ][0]
        for song in diffs.get("_difficultyBeatmaps", []):
            diff = song["_difficulty"]
            beatmap_file = song["_beatmapFilename"]
            data_song = self.read_file(f"{self.song_path}/{beatmap_file}")
            data_notes = pd.DataFrame(data_song.get("_notes", {}))
            data_obstacles = pd.DataFrame(data_song.get("_obstacles", {}))
            level_stats = pd.Series()
            level_stats["songHash"] = song_hash.upper()
            level_stats["diff"] = unify_diff(diff)
            level_stats = level_stats.append(
                self.calc_obstacles_stats(data_obstacles, bpm)
            )
            level_stats = level_stats.append(self.calc_notes_stats(data_notes, bpm))
            data_song_stats = data_song_stats.append(level_stats, ignore_index=True)
        return data_song_stats
