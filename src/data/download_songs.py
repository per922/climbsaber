# -*- coding: utf-8 -*-
import asyncio
import logging
from glob import glob
from io import BytesIO
from zipfile import ZipFile

import click

from src.beatsaver.api_client import BeatSaverAsync
from src.scoresaber.api_client import ScoreSaberAsync


async def download_song(song_hash: str, beatsaver: BeatSaverAsync, songs_folder: str):
    try:
        zip_bytes = await beatsaver.get_song(song_hash)
        with ZipFile(BytesIO(zip_bytes)) as zip:
            for file in zip.infolist():
                if file.filename.endswith(".dat"):
                    zip.extract(file, f"{songs_folder}/{song_hash}")
        return True
    except Exception as exception:
        logging.info(f"Can't process song with hash {song_hash}: {exception}")
        return False


async def fetch_songs():
    async with ScoreSaberAsync() as scoresaber:
        return await scoresaber.get_ranked_maps()


async def download_songs_async(songs_folder: str):
    logging.info("Using Scoresaber's API to get list of songs")
    songs = await fetch_songs()
    songs_hash_scoresaber = {song["songHash"].lower() for song in songs}
    logging.info(
        f"Got the list of {len(songs_hash_scoresaber)} ranked songs using Scoresaber API"
    )
    songs_hash_downloaded = {
        song_hash.split(f"{songs_folder}\\")[-1].split("_")[0].lower()
        for song_hash in glob(f"{songs_folder}/*")
    }
    songs_hash_to_download = songs_hash_scoresaber - songs_hash_downloaded
    logging.info(f"{len(songs_hash_to_download)} new maps founded. Starting download.")
    async with BeatSaverAsync() as beatsaver:
        processing_tasks = [
            download_song(song_hash, beatsaver, songs_folder)
            for song_hash in songs_hash_to_download
        ]
        were_collected = await asyncio.gather(*processing_tasks)
    logging.info(f"Finished downloading of {sum(were_collected)} songs")


@click.command()
@click.argument("songs_folder", type=click.Path())
def download_songs(songs_folder: str):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(download_songs_async(songs_folder))


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    download_songs()
