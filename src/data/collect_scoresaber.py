import asyncio
import logging

import click

from src.climbsaber.scoresaber import (
    ScoreSaberAsync,
    ScoreSaberCollector,
    ScoreSaberStorage,
)
from src.climbsaber.util import DBController


async def collect_songs(ss_collector: ScoreSaberCollector):
    await ss_collector.collect_songs()


async def collect_players_data(
    ss_collector: ScoreSaberCollector, page_start: int, page_end: int
):
    await ss_collector.collect_top_players(page_start, page_end)
    await ss_collector.collect_players_history()


async def collect_scoresaber_async(
    db_host: str,
    db_port: str,
    db_name: str,
    db_user: str,
    db_pass: str,
    page_start: int,
    page_end: int,
):
    ss_storage = ScoreSaberStorage(
        DBController(db_host, db_port, db_name, db_user, db_pass)
    )
    async with ScoreSaberAsync() as scoresaber:
        ss_collector = ScoreSaberCollector(ss_storage, scoresaber)
        await asyncio.gather(
            collect_songs(ss_collector),
            collect_players_data(ss_collector, page_start, page_end),
        )


@click.command()
@click.argument("db_host", type=click.STRING)
@click.argument("db_port", type=click.STRING)
@click.argument("db_name", type=click.STRING)
@click.argument("db_user", type=click.STRING)
@click.argument("db_pass", type=click.STRING)
@click.argument("page_start", type=click.INT)
@click.argument("page_end", type=click.INT)
def collect_scoresaber(
    db_host: str,
    db_port: str,
    db_name: str,
    db_user: str,
    db_pass: str,
    page_start: int,
    page_end: int,
):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(
        collect_scoresaber_async(
            db_host, db_port, db_name, db_user, db_pass, page_start, page_end
        )
    )


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    collect_scoresaber()
