# -*- coding: utf-8 -*-
import logging
import multiprocessing
from glob import glob
from pathlib import Path

import click
import pandas as pd
from tqdm.auto import tqdm

from src.climbsaber.song_parser import SongParser


def parse_song(song):
    song_parser = SongParser(song)
    return song_parser.parse_song()


@click.command()
@click.argument("songs_folder", type=click.Path(exists=True))
@click.argument("out_path", type=click.Path())
@click.argument("use_cache", type=click.BOOL)
def parse_songs(songs_folder: str, out_path: str, use_cache=True):
    if use_cache and Path(out_path).exists():
        data = pd.read_pickle(out_path)
    else:
        data = pd.DataFrame(columns=["songHash"])
    songs = set()
    for song_folder in tqdm(glob(f"{songs_folder}/*")):
        song_hash = song_folder.split("\\")[-1].split("_")[0]
        if (data["songHash"].str.lower() == song_hash.lower()).any():
            continue
        songs.add(song_folder)
    logging.info(f"start processing {len(songs)} maps")
    with multiprocessing.Pool(processes=multiprocessing.cpu_count()) as pool:
        parsed_songs = pool.map(parse_song, songs)
    data = pd.concat([data, *parsed_songs], axis=0)
    logging.info(f"{len(data)} levels processed")
    data.fillna(0).to_pickle(out_path)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    parse_songs()
