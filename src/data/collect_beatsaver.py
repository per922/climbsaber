import logging

import click

from src.climbsaber.beatsaver import BeatSaver


@click.command()
@click.argument("outpath", type=click.Path())
def collect_beatsaver(outpath: str):
    beatsaver = BeatSaver()
    beatsaver.process_scrapped_data(outpath)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    collect_beatsaver()
