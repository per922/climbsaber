# -*- coding: utf-8 -*-
import logging
import warnings

import click
import pandas as pd

from src.climbsaber.util import DBController

warnings.filterwarnings("always")
warnings.simplefilter("ignore")
logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.INFO)


def generate_players_stats(data, cols: list) -> pd.DataFrame:
    data_agg = data[cols].groupby("playerId").agg(["mean", "max", "min"])
    data_agg.columns = [
        f"player_{'_'.join([str(i) for i in col])}" for col in data_agg.columns
    ]
    return data_agg


@click.command()
@click.argument("songs_path", type=click.Path(exists=True))
@click.argument("db_host", type=click.STRING)
@click.argument("db_port", type=click.STRING)
@click.argument("db_name", type=click.STRING)
@click.argument("db_user", type=click.STRING)
@click.argument("db_pass", type=click.STRING)
@click.argument("beatsaver_path", type=click.Path(exists=True))
@click.argument("output_songs_info_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def generate_train_data(
    songs_path: str,
    db_host: str,
    db_port: str,
    db_name: str,
    db_user: str,
    db_pass: str,
    beatsaver_path: str,
    output_songs_info_path: str,
    output_path: str,
):
    """Runs data processing scripts to turn raw data from (../raw) into
    cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from all data")
    logger.info("read DB data")
    db = DBController(db_host, db_port, db_name, db_user, db_pass)
    data_players = db.get_data("SELECT * FROM players")
    data_songs = db.get_data("SELECT * FROM songs")
    data_playes = db.get_data("SELECT * FROM plays")
    logger.debug(
        "players",
        data_players.sample(3),
        "songs",
        data_songs.sample(3),
        "playes",
        data_playes.sample(3),
    )
    data_players.to_pickle(
        "data/interim/players.pkl"
    )  # required for neighbor predict method
    logger.info("read beatsaver data")
    data_beatsaver = pd.read_pickle(beatsaver_path)
    logger.debug("beatsaver", data_beatsaver.sample(3))
    logger.info("read song info data")
    data_dat = pd.read_pickle(songs_path)
    song_info_cols = data_dat.columns[2:].to_list()
    logger.debug("song info", data_dat.sample(3))
    logger.info("merge songs data")
    data_songs = data_songs.merge(
        data_beatsaver, how="left", on=["songHash", "diff"]
    ).drop_duplicates(subset=["songHash", "diff"], keep="first")
    data_songs = data_songs.merge(
        data_dat, how="left", on=["songHash", "diff"]
    ).drop_duplicates(subset=["songHash", "diff"], keep="first")
    del data_beatsaver, data_dat
    logger.info("save full song data to pickle")
    data_songs.attrs["song_info_cols"] = song_info_cols
    data_songs.to_pickle(output_songs_info_path)
    logger.info("clear plays data and add data about song")
    data_playes = data_playes[
        [
            "leaderboardId",
            "playerId",
            "modifiedScore",
            "badCuts",
            "missedNotes",
            "rank",
            "pp",
            "hmd",
            "weight",
        ]
    ]
    data_playes = data_playes.merge(
        data_songs, how="left", left_on="leaderboardId", right_on="id"
    )
    logger.info("add target column")
    data_playes["accuracy"] = data_playes["modifiedScore"] / data_playes["maxScore"] * 100
    logger.info("generate player based metrics")
    cols_player_to_agg = [
        "playerId",
        "pp",
        "accuracy",
        "npm",
        "stars",
        "duration",
        "rating",
        "obstacles",
    ] + song_info_cols
    data_players_agg = generate_players_stats(data_playes, cols_player_to_agg)
    cols_player_agg = data_players_agg.columns.to_list()
    logger.debug("players_agg_metrics", data_players_agg.sample(3))
    data_playes = data_playes.merge(
        data_players_agg, how="left", left_on="playerId", right_index=True
    )
    logger.info("calculate difference between player and levels stats")
    diff_cols = [
        col
        for col in data_playes
        if col.startswith("player_") and "pp" not in col and "accuracy" not in col
    ]
    for col in diff_cols:
        data_playes[f"diff_{col}"] = (
            data_playes[col] - data_playes["_".join(col.split("_")[1:-1])]
        )
    data_playes["stars_bin"] = pd.cut(
        data_playes["stars"], bins=[0, 2, 4, 6, 8, 10, 99]
    ).cat.codes
    cols_to_drop = (
        [
            "diff_player_stars_min",
            "diff_player_stars_max",
            "rank",
            "badCuts",
            "missedNotes",
            "pp",
            "modifiedScore",
            "weight",
            "id",
            "songHash",
            "songName",
            "songSubName",
            "songAuthorName",
            "levelAuthorName",
            "levelAuthorName",
            "diff",
            "coverImage",
            "rankedDate",
            "maxScore",
            "key",
        ]
        + song_info_cols
        + cols_player_agg
    )
    cols_to_drop.remove("player_pp_mean")  # keep feature :D
    data_playes = data_playes.drop(columns=cols_to_drop)
    logger.info(f"saving to file: {output_path}")
    data_playes.to_pickle(output_path)


if __name__ == "__main__":
    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    generate_train_data()
