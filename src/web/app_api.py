import base64
import json
from typing import Optional

import pandas as pd
from fastapi import Body, FastAPI
from fastapi.responses import FileResponse

from src.climbsaber.prediction import Predictor
from src.climbsaber.util import generate_playlist
from src.config import SONGS_FULL_DATA_PATH
from src.models.predict_model_implicit import ImplicitPredictor

app = FastAPI()
predictor = Predictor(songs_full_data_path=SONGS_FULL_DATA_PATH)


@app.get("/predict")
def predict_response(
    player_id: Optional[str], player_id2: Optional[str], count: Optional[int]
):
    if player_id is None:
        return "usage /predict?player_id=XXX&count=N or /predict?player_id=XXX&player_id2=XXX&count=N"
    if count is None:
        count = 6000
    try:
        data = predictor.prediction_pp(player_id, player_id2)
    except KeyError as e:
        return f"Player: {player_id} not found.\n{e}"

    star_upper = data[data["pp"] > 0]["stars"].max() + 0.5
    result = (
        data[data["stars"] < star_upper]
        .sort_values("pp_change", ascending=False)
        .head(count)
    )
    return result.to_json(orient="records", lines=True)


@app.get("/predict_implicit")
def predict_implicit_response(player_id: Optional[str]):
    if player_id is None:
        return "usage /predict_implicit?player_id=XXX"
    try:
        path_to_songs = "data/processed/data_song_full.pkl"
        path_to_model = "models/implicit.pickle"
        predictor_implicit = ImplicitPredictor(player_id, path_to_model, path_to_songs)
        data = predictor_implicit.predict()
    except KeyError as e:
        return f"Player: {player_id} not found.\n{e}"
    return data.to_json(orient="records", lines=True)


@app.get("/playlists")
def return_playlist(player_id: Optional[str]):
    if player_id is None:
        return "usage /playlists?player_id=XXX"
    return FileResponse(
        path=f"../../files/climbsaber_{player_id}.bplist",
        filename="climbsaber_{player_id}.bplist",
    )


@app.post("/generate_playlist")
def gen_playlist(content=Body()):
    player_id = content.get("player_id")
    data = content.get("data")
    if player_id and data:
        data = pd.DataFrame(json.loads(data))
        image = f"data:image/png;base64,{base64.b64encode(open('files/playlist_logo.png', 'rb').read()).decode()}"
        generate_playlist(
            data=data,
            title="Climbsaber",
            author="Duke_Perdolee",
            image=image,
            player_id=player_id,
        )
        return "Playlist generated"
    return "Playlist generation failed"
