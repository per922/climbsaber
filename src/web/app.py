import os
from typing import Any

import altair as alt
import extra_streamlit_components as stx
import numpy as np
import pandas as pd
import requests
import streamlit as st
from dotenv import load_dotenv

from src.climbsaber.scoresaber import ScoreSaber
from src.climbsaber.util import diff_to_text

load_dotenv()
API_URL = os.getenv("CLIMBSABER_API_URL")
alt.data_transformers.disable_max_rows()
st.set_page_config(
    page_title="ClimbSaber",
    page_icon="https://upload.wikimedia.org/wikipedia/commons/9/91/Beat_Saber_Logo.png",
    layout="wide",
)


def draw_rec(rec, based="ML", extra=False):
    if "pp_change" in rec.index:
        if (
            "pp_opponent" not in rec.index
            or np.isnan(rec["pp_change_opponent"])
            or based == "ML"
        ):
            pp_change = rec["pp_change"]
            predict = rec["predict"]
            potencial_pp = rec["potencial_pp"]
        else:
            pp_change = rec["pp_change_opponent"]
            predict = rec["accuracy_opponent"]
            potencial_pp = rec["pp_opponent"]
    else:
        pp_change = 0
    with st.expander(
        f"""[{rec['key']} {rec['diff']}] {rec['songAuthorName']} - {rec['songName']} 
                                {rec['songSubName']} (★{rec['stars']:.2f})     +{pp_change:.2f}pp""",
        expanded=extra,
    ):
        colsr = st.columns([1, 9])
        colsr[0].image(rec["coverImage"], width=100)
        colsr[1].write(
            f'[Download](https://eu.cdn.beatsaver.com/{rec["songHash"].lower()}.zip) | '
            f'[One-Click Install](beatsaver://{rec["key"]})'
        )
        colsr[1].write(f'Song hash: {rec["songHash"]}')
        if not extra and not np.isnan(rec["accuracy"]):
            colsr[1].write(
                f"You scored this song with {rec['accuracy']:.2f}% accuracy and received {rec['pp']:.2f} pp"
            )
        if not extra and not np.isnan(rec["potencial_pp"]):
            colsr[1].write(
                f"You may score with song with {predict:.2f}% accuracy "
                f"and receive {potencial_pp:.2f} pp"
            )
        if extra:
            colsr[1].button(
                label="Add to Playlist",
                key=f"button_{rec['songHash'].lower()}",
                on_click=save_state_buttons,
            )


def save_state_buttons():
    for key, value in st.session_state.items():
        if key.startswith("button_") and value is True:
            st.session_state[f"add_{key.split('_')[1]}"] = True


@st.cache(ttl=60 * 60, suppress_st_warning=True)
def request_api_predict(player, player2=None, count=4000):
    url = f"{API_URL}/predict?player_id={player}&count={count}"
    if player2:
        url += f"&player_id2={player2}"
    r = requests.get(url, timeout=240)
    print(r.url)
    if r.status_code != 200:
        st.error("Api is unavailable! Please contact me")
        st.stop()
    return pd.read_json(r.text, lines=True)


@st.cache(ttl=60 * 60, suppress_st_warning=True)
def request_api_implicit(player):
    url = f"{API_URL}/predict_implicit?player_id={player}"
    r = requests.get(url, timeout=240)
    print(r.url)
    if r.status_code != 200:
        st.error("Api is unavailable! Please contact me")
        st.stop()
    return pd.read_json(r.text, lines=True)


def request_api_generate_playlist(player, data_play):
    url = f"{API_URL}/generate_playlist"
    payload = {"player_id": player, "data": data_play.to_json(orient="records")}
    res = requests.post(url, json=payload)
    st.write(res.text)


def generate_plot(data_plot, col):
    data_plot_you = data_plot[data_plot["pp"] > 0].copy()
    data_plot_you.loc[:, "player"] = "You"
    if "pp_opponent" in data_plot.columns:
        data_plot_opp = data_plot[data_plot["pp_opponent"] > 0]
        data_plot_opp.loc[:, "player"] = "Opponent"
        data_plot_you = pd.concat([data_plot_you, data_plot_opp], axis=0)
    plot = (
        alt.Chart(data_plot_you[[col, "player"]])
        .mark_bar()
        .encode(
            x=alt.X(col, bin=alt.Bin(maxbins=30)),
            y=alt.Y("count()", stack=False),
            color="player",
        )
        .configure_mark(opacity=0.85)
    )
    st.altair_chart(plot, use_container_width=True)


def get_from_cookie(manager: stx.CookieManager, key: str, default: Any) -> str:
    return manager.get(key) if cookie_manager.get(key) else default


def save_to_cookie(manager: stx.CookieManager, key: str, value: Any):
    manager.set(key, value, key=f"cookie_{key}")


cookie_manager = stx.CookieManager()
# side menu
with st.sidebar:
    st.sidebar.image("files/banner.png", use_column_width=True)
    st.header("Climb Saber")
    with st.form(key="player_id_enter"):
        player_id = st.text_input(
            "Enter your ScoreSaber ID",
            max_chars=17,
            placeholder="Example: 76561198001658298",
            value=get_from_cookie(cookie_manager, "player_id", ""),
        )
        st.write("")
        if st.form_submit_button("GO!"):
            if len(player_id) != 17:
                st.error("Enter correct ScoreSaber ID")
            save_to_cookie(cookie_manager, "player_id", player_id)

if len(player_id):
    ss = ScoreSaber()
    player_info = ss.get_player_info(player_id)
    if len(player_info) == 0:
        st.error(f"Player {player_id} not found!")
        st.stop()
    cols = st.columns([2, 1, 9])
    cols[1].image(player_info["profilePicture"])
    cols[2].header(player_info["name"])
    cols[2].markdown(
        f"**PP**: {player_info['pp']} |"
        f" **RANK**: {player_info['rank']} ({player_info['countryRank']} "
        f"{player_info['country']})"
    )

    with st.spinner("Getting your play history and do some magic"):
        with st.form(key="player_id_compare_enter"):
            st.markdown(f"#### Compare to:")
            player_id_compare = st.text_input(
                "Enter your opponent ScoreSaber ID",
                max_chars=17,
                placeholder="Example: 76561198001658298",
                value=get_from_cookie(cookie_manager, "player_id_compare", ""),
            )
            st.write("")
            if st.form_submit_button("GO!"):
                if len(player_id) != 17:
                    st.error("Enter correct ScoreSaber ID")
                save_to_cookie(cookie_manager, "player_id_compare", player_id_compare)
            if player_id_compare:
                player_info_compare = ss.get_player_info(player_id_compare)
                if len(player_info) == 0:
                    st.error(f"Player {player_id_compare} not found!")
                    st.stop()
                cols = st.columns([2, 1, 9])
                cols[1].image(player_info_compare["profilePicture"])
                cols[2].header(player_info_compare["name"])
                cols[2].markdown(
                    f"**PP**: {player_info_compare['pp']} |"
                    f" **RANK**: {player_info_compare['rank']} ({player_info_compare['countryRank']} "
                    f"{player_info_compare['country']})"
                )
        try:
            if not player_id_compare:
                player_id_compare = None
            data = request_api_predict(player_id, player_id_compare)
        except requests.ConnectionError as e:
            st.error("Api is unavailable! Please contact me")
            st.stop()
    with st.expander(label="Click here to open/hide stats", expanded=True):
        st.markdown("#### **Stars** per song: ")
        generate_plot(data, "stars")
        st.markdown("#### **PP** per song: ")
        generate_plot(data, "pp")
        st.markdown("#### **Accuracy** per song: ")
        generate_plot(data, "accuracy")
        st.markdown("#### **NPM** per song: ")
        generate_plot(data, "npm")

    st.markdown("## My recommendations:")
    playlist_url = f"{API_URL}/playlists?player_id={player_id}"
    st.markdown(
        f"#### Download playlist: [Download]({playlist_url}) | "
        f"[One-Click Install](bsplaylist://playlist/{playlist_url})"
    )

    st.markdown(f"#### Settings: ")
    priority_options = ["ML", "Opponent"]
    priority = priority_options[0]
    if player_id_compare:
        priority = st.selectbox(
            label="pp change priority:",
            options=priority_options,
            index=int(get_from_cookie(cookie_manager, "priority", 0)),
        )
        save_to_cookie(cookie_manager, "priority", priority_options.index(priority))
    priority_map = {"ML": "pp_change", "Opponent": "pp_change_opponent"}
    pr_min_acc = float(get_from_cookie(cookie_manager, "pr_min_acc", 70.0))
    pr_max_acc = float(
        get_from_cookie(cookie_manager, "pr_max_acc", data["accuracy"].quantile(0.90))
    )
    pr_acc = st.slider(
        label="predicted accuracy in range: ",
        min_value=70.0,
        max_value=100.0,
        step=0.01,
        value=(pr_min_acc, pr_max_acc),
    )
    save_to_cookie(cookie_manager, "pr_min_acc", pr_acc[0])
    save_to_cookie(cookie_manager, "pr_max_acc", pr_acc[1])
    default_stars_min = float(data[data["pp"] > 0]["stars"].quantile(0.66))
    default_stars_max = float(data[data["pp"] > 0]["stars"].quantile(0.90))
    stars_min = float(get_from_cookie(cookie_manager, "stars_min", default_stars_min))
    stars_max = float(get_from_cookie(cookie_manager, "stars_max", default_stars_max))
    stars = st.slider(
        label="stars between: ",
        min_value=0.0,
        max_value=15.0,
        step=0.01,
        value=(stars_min, stars_max),
    )
    save_to_cookie(cookie_manager, "stars_min", stars[0])
    save_to_cookie(cookie_manager, "stars_max", stars[1])
    is_player_options = ["Ignore", "Drop", "Only"]
    is_player = st.selectbox(
        label="Already played:",
        options=is_player_options,
        index=int(get_from_cookie(cookie_manager, "is_player", 0)),
    )
    save_to_cookie(cookie_manager, "is_player", is_player_options.index(is_player))
    subset = data[
        (data["predict"].between(pr_acc[0], pr_acc[1]))
        & (data["stars"].between(stars[0], stars[1]))
    ].sort_values(priority_map[priority], ascending=False)
    if is_player == "Drop":
        subset = subset[data["pp"].isna()]
    if is_player == "Only":
        subset = subset[data["pp"].notna()]
    subset = subset.iloc[0:30]
    data_extra = request_api_implicit(player_id)
    extra_songs = [
        key.split("_")[1]
        for key, value in st.session_state.items()
        if key.startswith("add_") and value is True
    ]
    if extra_songs:
        subset = pd.concat(
            [subset, data_extra[data_extra["songHash"].str.lower().isin(extra_songs)]],
            axis=0,
        ).drop_duplicates(subset=["songHash"], keep="first")
    subset["diff"] = subset["diff"].apply(diff_to_text)

    with st.expander("Table", expanded=True):
        cols = [
            "songName",
            "songAuthorName",
            "diff",
            "stars",
            "songHash",
            "key",
            "accuracy",
            "pp",
        ]
        if player_id_compare and priority == "Opponent":
            cols.extend(["pp_change_opponent", "accuracy_opponent", "pp_opponent"])
        else:
            cols.extend(["pp_change", "predict", "potencial_pp"])
        st.dataframe(subset[cols])

    if len(subset):
        subset.apply(lambda x: draw_rec(x, priority), axis=1)
        st.markdown("---")
        request_api_generate_playlist(player=player_id, data_play=subset.fillna(0))
    if len(data_extra):
        st.markdown("---")
        st.markdown("## Maybe You Will Like It:")
        data_extra[~data_extra["id"].isin(subset["id"])].drop_duplicates(
            subset="songHash"
        ).iloc[:30].apply(lambda x: draw_rec(x, priority, extra=True), axis=1)
